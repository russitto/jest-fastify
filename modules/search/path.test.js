const app = require('../../app')

test('/search path', async () => {
  const response = await app.inject({
    method: 'GET',
    url: '/search',
    query: { str: 'pacman' },
  })
  expect(response.statusCode).toBe(200)
  expect(response.headers['content-type']).toMatch(/^application\/json/i)
  const payload = JSON.parse(response.body)
  expect(typeof payload).toBe('object')
  expect(payload.results.length).toBeGreaterThan(0)
})

test('/search path unkown package', async () => {
  const response = await app.inject({
    method: 'GET',
    url: '/search',
    query: { str: 'caasdfadsfasdfqwerqwerqwerrwqerqwerewpn' },
  })
  expect(response.statusCode).toBe(200)
  expect(response.headers['content-type']).toMatch(/^application\/json/i)
  const payload = JSON.parse(response.body)
  expect(typeof payload).toBe('object')
  expect(payload.results.length).toBe(0)
})
