const service = require('./service.js')

module.exports = (app) => {
  const searchSchema = {
    summary: 'Search packages',
    description: 'Search official in archlinux repos',
    operationId: 'search',
    tags: ['search'],
    querystring: {
      type: 'object',
      properties: {
        str: {
          type: 'string',
          description: 'String to search',
          minLength: 1,
        },
      },
      required: [
        'str',
      ],
      example: {
        str: 'pacman',
      },
    },
    response: {
      200: {
        description: 'package list',
        type: 'array',
        items: {
          type: 'object',
        },
      }
    }
  }

  app.get('/search', searchSchema, search)

  async function search(request, reply) {
    const { statusCode, body } = await service(request.query.str)
    return body.json()
  }

  return {
    search, searchSchema
  }
}
