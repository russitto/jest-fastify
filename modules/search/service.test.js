const service = require('./service')

test('search service', async () => {
  const { body } = await service('mysql')
  const payload = await body.json()
  expect(payload.results.length).toBeGreaterThan(0)
  expect(payload.results[0].pkgdesc).toMatch(/mysql/i)
})
