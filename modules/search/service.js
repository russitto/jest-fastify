const { request } = require('undici')

const BASE = 'https://archlinux.org/packages/search/json/?q='

module.exports = search

async function search(str) {
  const {
    statusCode,
    headers,
    trailers,
    body
  } = await request(`${BASE}${str}`)
  return {
    statusCode, body
  }
}
