const search = require('./search/path')

module.exports = (app) => {
  return search(app)
}
