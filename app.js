const fastify = require('fastify')

const modules = require('./modules')

const app = fastify({
  logger: true,
})

modules(app)

module.exports = app
